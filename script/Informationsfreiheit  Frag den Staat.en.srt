1
00:00:04,108 --> 00:00:05,778
With the Freedom of Information Act

2
00:00:05,918 --> 00:00:09,025
citizens can receive information from authorities

3
00:00:09,225 --> 00:00:11,511
these authorities publish a lot of important information

4
00:00:11,511 --> 00:00:13,927
but by no means everything!

5
00:00:13,927 --> 00:00:16,083
the Freedom of Information Act allows us

6
00:00:16,083 --> 00:00:18,259
to look into unpublished files and documents

7
00:00:20,133 --> 00:00:22,275
when you use the FOI

8
00:00:22,444 --> 00:00:25,735
you make the work of officials more public and transparent.

9
00:00:27,371 --> 00:00:29,585
any person may ask for information

10
00:00:29,585 --> 00:00:31,465
without giving a particular reason

11
00:00:31,465 --> 00:00:32,931
the authorities have to answer them.

12
00:00:33,673 --> 00:00:37,230
through the FOI you can request various types of documents

13
00:00:37,542 --> 00:00:40,106
for example: building plans, correspondence

14
00:00:40,106 --> 00:00:43,023
data sets, or internal meeting records.

15
00:00:44,954 --> 00:00:47,458
without information it can sometimes be difficult to be heard

16
00:00:49,335 --> 00:00:51,301
with a Freedom of Information request

17
00:00:51,411 --> 00:00:53,429
you can support your cause with evidence

18
00:00:57,270 --> 00:01:00,884
information distributed throughout different parts of public administration

19
00:01:01,054 --> 00:01:04,141
can be combined to uncover interesting stories

20
00:01:07,668 --> 00:01:10,171
for all these cases there's FragDenStaat.de

21
00:01:10,668 --> 00:01:13,960
the site makes it easy to submit FOI requests.

22
00:01:14,778 --> 00:01:16,627
First you select the authority

23
00:01:16,627 --> 00:01:19,237
who might have the information you require

24
00:01:20,212 --> 00:01:22,031
in the next step, you briefly describe

25
00:01:22,031 --> 00:01:24,383
which information you specifically require.

26
00:01:25,183 --> 00:01:29,673
[FOI request about airport plans + noise]

27
00:01:30,553 --> 00:01:34,183
we package your application in a preformatted FOI request

28
00:01:34,324 --> 00:01:36,804
and send it to the authority on your behalf

29
00:01:37,589 --> 00:01:40,009
your request, together with the answer from the authority

30
00:01:40,119 --> 00:01:42,726
will be published on FragDenStaat.de

31
00:01:43,337 --> 00:01:45,817
this way, you can share the information with other interested parties

32
00:01:46,219 --> 00:01:49,065
and drive attention to your cause or concern

33
00:01:49,065 --> 00:01:51,703
FragDenStaat is a free, nonprofit service

34
00:01:51,703 --> 00:01:53,796
from Open Knowledge Foundation Germany

35
00:01:53,926 --> 00:01:55,802
Help to shine light into public administration

36
00:01:56,132 --> 00:01:58,026
Frag den Staat! (ask the state)

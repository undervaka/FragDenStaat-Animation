## Scene 10: laptop.sif

## Voiceover:

"for all these cases there is FragDenStaat.de

the site makes it easy to submit FOI requests.

GOVERNMENT DEPARTMENTS, LOCAL / STATE / FEDERAL:  
First you select the authority who might have the information you require.

REQUESTING INFORMATION ON NEW AIRPORT:  
in the next step, you briefly describe which informaiton you specifically require.

we package your application in a preformatted FOI request and send it to the authority on your behalf.

PUBLISHED FOI REQUEST + LAPTOPS:  
your request, together with the answer from the authority, will be published on FragDenStaat.de - this way, you can share the information with other interested parties, and drive attention to your cause or concern.
on distributed throughout different parts of public administration can be combined to uncover interesting stories."

## Scene Description:

We follow a simplified version of submitting an FOI request through Frag den Staat: 

![url](http://www.cameralibre.cc/wp-content/uploads/url-300x169.jpg)

type in the URL 

![department](http://www.cameralibre.cc/wp-content/uploads/laptop-300x169.jpg)  ![level](http://www.cameralibre.cc/wp-content/uploads/bundlandstadt-300x169.jpg)

choose which authority and governmental level you would like to address

![request](http://www.cameralibre.cc/wp-content/uploads/bitte_schicken-300x169.jpg)

type your request for information. We see a reference to the legal text which is automated by the website. 

![tick](http://www.cameralibre.cc/wp-content/uploads/tick-300x169.jpg)

The user clicks 'Send' and waits - the clock hands rotate and turn into a tick. 

![response](http://www.cameralibre.cc/wp-content/uploads/response-300x169.jpg)

The response, together with the original request, is published on the website - we see, for example, a text document, a diagram of flight paths and estimated noise levels, and some graphs.

![sharing](http://www.cameralibre.cc/wp-content/uploads/shared-300x169.jpg)

The laptop pulls back and we see many more computers appear, displaying the same information.

## Changes for international contexts:  

This scene has a lot of text, so there are obviously also a lot of changes required.
I have lost track of my final images for choosing Federal (Reichstag building), State (map of German states) or City (Hamburg Rathaus), but these are elements which should be changed for internationalisation anyway, so it's not such a problem.
Other changes:
- The main font used should match the of the local website's headings, logos etc.
- laptop/Laptop body/www.FragdenStaat.de: URL of local website
- laptop/Screen_(masked)/different authorities: text for Education, Justice, Construction, Transport, Health
- laptop/Screen_(masked)/CountryStateCity : text for Federal/National, State, and City government levels
- laptop/Screen_(masked)/FOI request (Information zu...): text of the FOI request. The German text reads: 
### Information Regarding the Airport
_Please send me all documents regarding:_
     - _Flight paths_
     - _Flight scheduling_
     - _Noise level data_
_for the planned ABC Airport_
- laptop/Screen_(masked)/legal smallprint text: the dense legal language which is automated by the website - I have copy and pasted a simplified version of the real legal text which is produced by Frag den Staat, and I suggest you do something similar for localisation (Though I doubt any language can beat German if you want dense legal jargon!)
- laptop/Screen_(masked)/Absenden: 'Send' button

- text for Cit

